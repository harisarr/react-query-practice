import React, { useState } from "react";
import {
  Box,
  Flex,
  Text,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  Textarea,
} from "@chakra-ui/react";
import Layout from "../../components/Layout";
import { useMutation, useQuery, useQueryClient } from "react-query";
import MamaTable from "./MamaTable";
import { useForm } from "react-hook-form";

const getMessages = async () => {
  const URL = "http://localhost:3000/api/message";
  const result = await fetch(URL);
  return await result.json();
};

export type MessagesProps = {
  id?: number;
  createdAt: string;
  phoneNumber: number;
  message: string;
  status?: string;
};

export function formatDate(date: string) {
  return new Date(date).toLocaleString("id-ID");
}

const submitMessage = async (data: MessagesProps) => {
  const URL = "http://localhost:3000/api/message";
  const response = await fetch(URL, {
    method: "POST",
    body: JSON.stringify(data),
  });

  if (!response.ok) {
    throw new Error("An Error has occured");
  }
  return await response.json();
};

export default function MamaMuda() {
  const queryClient = useQueryClient();
  const { data, isSuccess } = useQuery("get-mama-muda", getMessages, {
    staleTime: 15000,
    refetchInterval: 15000,
  });
  const [, setErrMessage] = useState("");

  const {
    handleSubmit,
    errors,
    register,
    reset,
    clearErrors,
  } = useForm<MessagesProps>();
  const mutation = useMutation(submitMessage, {
    onMutate: async (newMessage) => {
      //    1. cancel any outgoing refetch
      await queryClient.cancelQueries("get-mama-muda");
      //    2. snapshot the previous cache
      const previousMessages = queryClient.getQueryData<MessagesProps[]>(
        "get-mama-muda"
      );
      //    3. optimistically update new value
      if (previousMessages) {
        newMessage = { ...newMessage, createdAt: new Date().toISOString() };
        const finalMessages = [...previousMessages, newMessage];
        queryClient.setQueryData("get-mama-muda", finalMessages);
      }

      return { previousMessages };
    },
    onSettled: async (data, error: any) => {
      console.log("onSettled");
      if (data) {
        await queryClient.invalidateQueries("get-mama-muda");
        setErrMessage("");
        reset();
        clearErrors();
      }
      if (error) {
        setErrMessage(error.message);
      }
    },
    onError: (error: any, _variables, context: any) => {
      // console.log("onError");
      setErrMessage(error.message);
      if (context?.previousMessage) {
        queryClient.setQueryData<MessagesProps[]>(
          "get-mama-muda",
          context.previousMessage
        );
      }
    },
    onSuccess: () => {
      console.log("onSuccess");
    },
  });

  const onSubmit = async (data: MessagesProps) => {
    await mutation.mutate(data);
  };
  return (
    <Layout title="💌 Mama Muda" subTitle="Minta Pulsa">
      <Flex>
        <Box>
          <Box
            w="md"
            p={5}
            mr={4}
            border="1px"
            borderColor="gray.200"
            boxShadow="md"
          >
            <Text
              fontSize="xl"
              fontWeight="bold"
              mb={4}
              pb={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              ✍️ Request Pulsa
            </Text>
            <form>
              <FormControl pb={4} isInvalid={errors.phoneNumber ? true : false}>
                <FormLabel
                  htmlFor="phoneNumber"
                  fontWeight="bold"
                  fontSize="xs"
                  letterSpacing="1px"
                  textTransform="uppercase"
                >
                  Phone Number
                </FormLabel>
                <Input
                  name="phoneNumber"
                  placeholder="Phone Number"
                  ref={register({
                    required: "Phone Number Required",
                  })}
                />
                <FormErrorMessage>
                  {errors.phoneNumber && errors.phoneNumber.message}
                </FormErrorMessage>
              </FormControl>

              <FormControl isInvalid={errors.message ? true : false}>
                <FormLabel
                  htmlFor="name"
                  fontWeight="bold"
                  fontSize="xs"
                  letterSpacing="1px"
                  textTransform="uppercase"
                >
                  Message
                </FormLabel>
                <Textarea
                  placeholder="Bullshit Message"
                  name="message"
                  ref={register({
                    required: "Message Required",
                  })}
                />
                <FormErrorMessage>
                  {errors.message && errors.message.message}
                </FormErrorMessage>
              </FormControl>

              <Button
                mt={4}
                colorScheme="teal"
                type="submit"
                onClick={handleSubmit(onSubmit)}
              >
                Send
              </Button>
            </form>
          </Box>
        </Box>
        <Box flex="1">{isSuccess && <MamaTable data={data} />}</Box>
      </Flex>
    </Layout>
  );
}

// NOTE
// [] onMutated => executed when mutation in progress, used for : spinner, disabled eform, etc
// [] onSettled => executed when mutation is done, result could be success{data in data} || error{data in error}
// [] onError => executed when mutation is done with error response
// [] onSucces => executed when mutation is done with success response
// [] Optimistic Update :
//    1. cancel any outgoing refetch
//    2. snapshot the previous cache
//    3. optimistically update new value
